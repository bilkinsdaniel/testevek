import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TaxaSimuladorPermitidaComponent } from './taxa-simulador-permitida.component';

describe('TaxaSimuladorPermitidaComponent', () => {
  let component: TaxaSimuladorPermitidaComponent;
  let fixture: ComponentFixture<TaxaSimuladorPermitidaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TaxaSimuladorPermitidaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TaxaSimuladorPermitidaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
