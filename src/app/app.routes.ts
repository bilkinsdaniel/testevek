import { Routes } from "@angular/router";

import { HomeComponent } from "./home/home.component";
import { DadosClienteComponent } from "./dados-cliente/dados-cliente.component";
import { InfoTaxasComponent } from "./info-taxas/info-taxas.component";
import { TaxaSimuladorPermitidaComponent } from "./taxa-simulador-permitida/taxa-simulador-permitida.component";
import { TaxaSimuladorNaoPermitidaComponent } from "./taxa-simulador-nao-permitida/taxa-simulador-nao-permitida.component";

export const ROUTES : Routes = [
    {path:'', component: HomeComponent},
    {path:'dados_cliente', component: DadosClienteComponent},
    {path:'dados_cliente/:objPesquisa', component: DadosClienteComponent},
    {path:'info_taxas', component: InfoTaxasComponent},
    {path:'taxa_permitida', component: TaxaSimuladorPermitidaComponent},
    {path:'taxa_nao_permitida', component: TaxaSimuladorNaoPermitidaComponent}
]