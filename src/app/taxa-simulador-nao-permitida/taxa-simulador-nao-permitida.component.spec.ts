import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TaxaSimuladorNaoPermitidaComponent } from './taxa-simulador-nao-permitida.component';

describe('TaxaSimuladorNaoPermitidaComponent', () => {
  let component: TaxaSimuladorNaoPermitidaComponent;
  let fixture: ComponentFixture<TaxaSimuladorNaoPermitidaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TaxaSimuladorNaoPermitidaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TaxaSimuladorNaoPermitidaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
