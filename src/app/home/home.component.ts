import { Component, OnInit } from '@angular/core';
import { Pesquisa } from "../shared/pesquisa.model";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  public dadosPesquisa: Pesquisa = new Pesquisa();

  constructor() { }

  ngOnInit(): void {
    //console.log(this.dadosPesquisa);
  }

}
