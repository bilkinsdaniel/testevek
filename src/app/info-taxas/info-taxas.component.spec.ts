import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InfoTaxasComponent } from './info-taxas.component';

describe('InfoTaxasComponent', () => {
  let component: InfoTaxasComponent;
  let fixture: ComponentFixture<InfoTaxasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InfoTaxasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InfoTaxasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
