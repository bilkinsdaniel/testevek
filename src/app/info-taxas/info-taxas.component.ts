import { Component, OnInit } from '@angular/core';
import { Concorrentes } from "../shared/concorrente.model";

@Component({
  selector: 'app-info-taxas',
  templateUrl: './info-taxas.component.html',
  styleUrls: ['./info-taxas.component.css']
})
export class InfoTaxasComponent implements OnInit {

  public concorrentes: Concorrentes[] = [
    {id:1, valor:"Concorrete1"},
    {id:2, valor:"Concorrete2"},
    {id:3, valor:"Concorrete3"}
  ]

  constructor() { }

  ngOnInit(): void {
  }

}
