import { Component, OnInit } from '@angular/core';
import { RamoAtividade } from "../shared/ramo-atividade.model";
import { ActivatedRoute } from "@angular/router";
import { Pesquisa } from "../shared/pesquisa.model";

@Component({
  selector: 'app-dados-cliente',
  templateUrl: './dados-cliente.component.html',
  styleUrls: ['./dados-cliente.component.css']
})
export class DadosClienteComponent implements OnInit {

  public cpf: string;
  public email: string;
  public ramoAtividade: string;
  public telefone: string;

  //public pesquisaDados: Pesquisa;
  private route: ActivatedRoute;

  public ramosAtividade: RamoAtividade[] = [
    {id:1, valor: "Atividade 1"},
    {id:2, valor: "Atividade 2"}
  ]

  constructor() {
  
   }

  ngOnInit(): void {
    //console.log(this.pesquisaDados);
    //console.log(this.route.snapshot.params["objPesquisa"]);
    //this.pesquisaDados = new Pesquisa();

    this.route.params.subscribe((parametros:Pesquisa) => {
      //this.cpf = parametros.cpf;
      //this.email = parametros.email;
      //this.ramoAtividade = parametros.ramoAtividade;
      //this.telefone = parametros.telefone;
      console.log(parametros);
    });
    
  }

  public gravaCpf(dadosCpf:Event): void{
    this.cpf = (<HTMLInputElement>dadosCpf.target).value;
  }
  public gravaTelefone(dadosTelefone:Event): void{
    this.telefone = (<HTMLInputElement>dadosTelefone.target).value;
  }
  public gravaEmail(dadosEmail:Event): void{
    this.email = (<HTMLInputElement>dadosEmail.target).value;
  }
  public gravaRamoAtividade(dadosRamoAtividade:Event): void{
    this.ramoAtividade = (<HTMLInputElement>dadosRamoAtividade.target).value;
  }
}
