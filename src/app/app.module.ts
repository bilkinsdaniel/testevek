import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from "@angular/router";

import { ROUTES } from "./app.routes"; 

import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { DadosClienteComponent } from './dados-cliente/dados-cliente.component';
import { InfoTaxasComponent } from './info-taxas/info-taxas.component';
import { TaxaSimuladorPermitidaComponent } from './taxa-simulador-permitida/taxa-simulador-permitida.component';
import { TaxaSimuladorNaoPermitidaComponent } from './taxa-simulador-nao-permitida/taxa-simulador-nao-permitida.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    DadosClienteComponent,
    InfoTaxasComponent,
    TaxaSimuladorPermitidaComponent,
    TaxaSimuladorNaoPermitidaComponent,
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(ROUTES)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
